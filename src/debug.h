#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <GL/glew.h>

static const char *glErrorValue_(GLenum errorCode)
{
  switch (errorCode) {
    case GL_INVALID_ENUM:                  return "INVALID_ENUM";
    case GL_INVALID_VALUE:                 return "INVALID_VALUE";
    case GL_INVALID_OPERATION:             return "INVALID_OPERATION";
    case GL_STACK_OVERFLOW:                return "STACK_OVERFLOW";
    case GL_STACK_UNDERFLOW:               return "STACK_UNDERFLOW";
    case GL_OUT_OF_MEMORY:                 return "OUT_OF_MEMORY";
    case GL_INVALID_FRAMEBUFFER_OPERATION: return "INVALID_FRAMEBUFFER_OPERATION";
    default:                               return "";
  }
}

static GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR) {
        const char *error = glErrorValue_(errorCode);
        printf("%s | %s (%d)\n", error, file, line);
    }
    return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__) 

#endif
