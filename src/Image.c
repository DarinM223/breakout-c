#include "Image.h"
#include "stb_image.h"

ImageError initImage(const char *path, Image *image)
{
  int width, height, nrComponents;
  unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
  if (data == NULL) {
    return IMAGE_LOAD_ERROR;
  }

  image->data = data;
  image->width = width;
  image->height = height;
  image->nrComponents = nrComponents;
  return IMAGE_NO_ERROR;
}

void freeImage(Image image)
{
  stbi_image_free(image.data);
}
