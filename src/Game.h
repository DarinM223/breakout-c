#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <stdbool.h>
#include "SpriteRenderer.h"

typedef enum GameState {
  STATE_ACTIVE,
  STATE_MENU,
  STATE_WIN,
} GameState;

typedef struct Game {
  bool keys[1024];
  GameState state;
  GLfloat dt;
  GLfloat lastTime;
  int width;
  int height;
  GLuint texture;
  SpriteRenderer renderer;
} Game;

Game *initGame(int width, int height);
void gameInitData(Game *game);
void gameProcessInput(Game *game);
void gameUpdate(Game *game);
void gameRender(const Game *game);
void gameUpdateTime(Game *game, GLfloat time);
void gameSetKey(Game *game, int key, bool value);
void freeGame(Game *game);

#endif
