#define GLEW_STATIC
#define GLFW_INCLUDE_GLCOREARB
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Game.h"
#include <kazmath/kazmath.h>
#include "debug.h"

const GLuint SCREEN_WIDTH = 800;
const GLuint SCREEN_HEIGHT = 600;

void keyCallback(GLFWwindow *, int, int, int, int);

Game *breakout;

int main()
{
  glfwInit();
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  GLFWwindow *window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Breakout", NULL, NULL);
  glfwMakeContextCurrent(window);

  glewExperimental = GL_TRUE;
  glewInit();
  glGetError();

  breakout = initGame(SCREEN_WIDTH, SCREEN_HEIGHT);
  glfwSetKeyCallback(window, keyCallback);

  glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
  glEnable(GL_CULL_FACE);
  glCheckError();
  glEnable(GL_BLEND);
  glCheckError();
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glCheckError();

  gameInitData(breakout);

  while (!glfwWindowShouldClose(window)) {
    gameUpdateTime(breakout, glfwGetTime());

    glfwPollEvents();
    gameProcessInput(breakout);

    gameUpdate(breakout);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glCheckError();
    gameRender(breakout);

    glfwSwapBuffers(window);
  }

  freeGame(breakout);
  glfwTerminate();
  return 0;
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }
  if (key >= 0 && key < 1024) {
    if (action == GLFW_PRESS) {
      gameSetKey(breakout, key, GL_TRUE);
    } else if (action == GLFW_RELEASE) {
      gameSetKey(breakout, key, GL_FALSE);
    }
  }
}
