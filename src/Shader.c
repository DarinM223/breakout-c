#include <stdio.h>
#include "Shader.h"
#include "debug.h"

static ShaderError loadShaderSrcs(const char *vertexPath, const char *fragmentPath, const char **vertexSrc, const char **fragmentSrc)
{
  ShaderError err = { .type = SHADER_NO_ERROR };
  FILE *vertexFile = fopen(vertexPath, "r");
  if (vertexFile == NULL) {
    err.type = SHADER_LOAD_ERROR;
    err.loadError.vertexPath = vertexPath;
    err.loadError.fragmentPath = fragmentPath;
    return err;
  }

  FILE *fragmentFile = fopen(fragmentPath, "r");
  if (fragmentFile == NULL) {
    err.type = SHADER_LOAD_ERROR;
    err.loadError.vertexPath = vertexPath;
    err.loadError.fragmentPath = fragmentPath;
    goto cleanup_vertex;
  }

  // Read from file into buffers.
  char vertexBuf[10000], fragmentBuf[10000];
  fread(vertexBuf, 1, 1000, vertexFile);
  fread(fragmentBuf, 1, 1000, fragmentFile);
  *vertexSrc = vertexBuf;
  *fragmentSrc = fragmentBuf;

  fclose(fragmentFile);
cleanup_vertex:
  fclose(vertexFile);
  return err;
}

ShaderError initShader(const char *vertexPath, const char *fragmentPath, GLuint *shader)
{
  const char *vertexSrc, *fragmentSrc;
  ShaderError err = loadShaderSrcs(vertexPath, fragmentPath, &vertexSrc, &fragmentSrc);
  if (err.type != SHADER_NO_ERROR) {
    return err;
  }

  GLint success;
  GLchar infoLog[512];

  GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexSrc, NULL);
  glCheckError();
  glCompileShader(vertexShader);
  glCheckError();
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    err.type = SHADER_COMPILE_ERROR;
    err.compileError.vertex = true;
    err.compileError.log = infoLog;
    goto cleanup_vertex;
  }

  GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentSrc, NULL);
  glCheckError();
  glCompileShader(fragmentShader);
  glCheckError();
  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    err.type = SHADER_COMPILE_ERROR;
    err.compileError.vertex = false;
    err.compileError.log = infoLog;
    goto cleanup_fragment;
  }

  *shader = glCreateProgram();
  glAttachShader(*shader, vertexShader);
  glCheckError();
  glAttachShader(*shader, fragmentShader);
  glCheckError();
  glLinkProgram(*shader);
  glCheckError();
  glGetProgramiv(*shader, GL_LINK_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(*shader, 512, NULL, infoLog);
    err.type = SHADER_LINK_ERROR;
    err.linkError.log = infoLog;
    goto cleanup_fragment;
  }

cleanup_fragment:
  glDeleteShader(fragmentShader);
cleanup_vertex:
  glDeleteShader(vertexShader);

  return err;
}
