#ifndef IMAGE_H
#define IMAGE_H

typedef enum ImageError {
  IMAGE_NO_ERROR,
  IMAGE_LOAD_ERROR,
} ImageError;

typedef struct Image {
  unsigned char *data;
  int width;
  int height;
  int nrComponents;
} Image;

ImageError initImage(const char *path, Image *image);
void freeImage(Image image);

#endif
