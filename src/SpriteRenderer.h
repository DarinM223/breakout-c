#ifndef SPRITE_RENDERER_H
#define SPRITE_RENDERER_H

#define GLFW_INCLUDE_GLCOREARB
#include <GL/glew.h>
#include <kazmath/kazmath.h>
#include "Texture.h"

typedef struct SpriteRendererOptions {
  kmVec2 position;
  kmVec2 size;
  GLfloat rotate;
  kmVec3 color;
} SpriteRendererOptions;

typedef struct SpriteRenderer {
  GLuint shader;
  GLuint vao;
} SpriteRenderer;

SpriteRenderer initSpriteRenderer(GLuint shader);
void spriteRendererDrawSprite(SpriteRenderer renderer,
                              GLuint texture,
                              SpriteRendererOptions options);
void freeSpriteRenderer(SpriteRenderer renderer);

#endif
