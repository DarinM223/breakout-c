#include <stdio.h>
#include <stdlib.h>
#include "Game.h"
#include "Image.h"
#include "Texture.h"
#include "Shader.h"
#include "debug.h"

Game *initGame(int width, int height)
{
  Game *game = (Game *) malloc(sizeof(Game));
  game->width = width;
  game->height = height;
  game->state = STATE_ACTIVE;
  game->dt = 0.0f;
  game->lastTime = 0.0f;
  for (size_t i = 0; i < 1024; i++) {
    game->keys[i] = false;
  }
  return game;
}

void gameInitData(Game *game)
{
  GLuint shader;
  ShaderError shaderErr = initShader("./shaders/sprite.vs", "./shaders/sprite.frag", &shader);
  if (shaderErr.type != SHADER_NO_ERROR) {
    printf("Error loading shader\n");
  }
  kmMat4 projection;
  kmMat4Identity(&projection);
  kmMat4OrthographicProjection(&projection, 0.0f, game->width, game->height, 0.0f, -1.0f, 1.0f);

  glUseProgram(shader);
  glUniform1i(glGetUniformLocation(shader, "image"), 0);
  glCheckError();

  glUniformMatrix4fv(glGetUniformLocation(shader, "projection"), 1, GL_FALSE, &projection.mat[0]);
  glCheckError();

  game->renderer = initSpriteRenderer(shader);

  Dimensions dimensions = { .type = IMAGE_DIMENSIONS };
  TextureOptions options = textureOptionsDefault();
  ImageError imgErr = initTexture("./assets/awesomeface.png", dimensions, options, &game->texture);
  if (imgErr == IMAGE_LOAD_ERROR) {
    printf("Error loading image\n");
  }
}

void gameProcessInput(Game *game)
{
  // TODO(DarinM223): process input in game.
}

void gameUpdate(Game *game)
{
  // TODO(DarinM223): update game.
}

void gameRender(const Game *game)
{
  // TODO(DarinM223): render game.
  kmVec2 position = { .x = 200, .y = 200 };
  kmVec2 size = { .x = 300, .y = 400 };
  kmVec3 color = { .x = 0.0f, .y = 1.0f, .z = 0.0f };
  SpriteRendererOptions options = {
    .position = position,
    .size = size,
    .rotate = 45.0f,
    .color = color,
  };
  spriteRendererDrawSprite(game->renderer, game->texture, options);
}

void gameUpdateTime(Game *game, GLfloat time)
{
  GLfloat dt = time - game->lastTime;
  game->dt = dt;
  game->lastTime = time;
}

void gameSetKey(Game *game, int key, bool value)
{
  game->keys[key] = value;
}

void freeGame(Game *game)
{
  if (game != NULL) {
    freeSpriteRenderer(game->renderer);
    glDeleteTextures(1, &game->texture);
  }
  free(game);
}
