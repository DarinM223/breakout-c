#include "Texture.h"
#include "debug.h"

TextureOptions textureOptionsDefault()
{
  TextureOptions options = {
    .wrapS = GL_REPEAT,
    .wrapT = GL_REPEAT,
    .internalFormat = GL_RGBA,
    .imageFormat = GL_RGBA,
    .filterMin = GL_LINEAR,
    .filterMag = GL_LINEAR,
  };
  return options;
}

ImageError initTexture(const char *path,
                       Dimensions dimensions,
                       TextureOptions options,
                       GLuint *texture)
{
  glGenTextures(1, texture);
  glCheckError();
  glBindTexture(GL_TEXTURE_2D, *texture);
  glCheckError();
  Image image;
  ImageError err = initImage(path, &image);
  if (err != IMAGE_NO_ERROR) {
    goto cleanup;
  }

  int width, height;
  if (dimensions.type == CUSTOM_DIMENSIONS) {
    width = dimensions.width;
    height = dimensions.height;
  } else {
    width = image.width;
    height = image.height;
  }
  glTexImage2D(GL_TEXTURE_2D, 0, options.internalFormat, width, height, 0, options.imageFormat, GL_UNSIGNED_BYTE, image.data);
  glCheckError();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, options.wrapS);
  glCheckError();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, options.wrapT);
  glCheckError();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, options.filterMin);
  glCheckError();
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, options.filterMag);
  glCheckError();

  freeImage(image);
cleanup:
  glBindTexture(GL_TEXTURE_2D, 0);
  return err;
}
