#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include "Image.h"

typedef enum DimensionsType {
  IMAGE_DIMENSIONS,
  CUSTOM_DIMENSIONS,
} DimensionsType;

typedef struct Dimensions {
  DimensionsType type;
  int width;
  int height;
} Dimensions;

typedef struct TextureOptions {
  GLint wrapS;
  GLint wrapT;
  GLint internalFormat;
  GLint imageFormat;
  GLint filterMin;
  GLint filterMag;
} TextureOptions;

TextureOptions textureOptionsDefault();
ImageError initTexture(const char *path,
                       Dimensions dimensions,
                       TextureOptions options,
                       GLuint *texture);

#endif
