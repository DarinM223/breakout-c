#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <stdbool.h>

typedef enum ShaderErrorType {
  SHADER_NO_ERROR,
  SHADER_LOAD_ERROR,
  SHADER_COMPILE_ERROR,
  SHADER_LINK_ERROR,
} ShaderErrorType;

typedef struct ShaderError {
  ShaderErrorType type;
  union {
    struct {
      const char *vertexPath;
      const char *fragmentPath;
    } loadError;

    struct {
      bool vertex;
      const char *log;
    } compileError;

    struct { const char *log; } linkError;
  };
} ShaderError;

ShaderError initShader(const char *vertexPath, const char *fragmentPath, GLuint *shader);

#endif
