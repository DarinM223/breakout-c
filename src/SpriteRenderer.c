#include "SpriteRenderer.h"
#include "debug.h"

static void initRenderData(SpriteRenderer *renderer)
{
  GLuint vbo;
  GLfloat vertices[] = {
    0.0f, 1.0f, 0.0f, 1.0f,
    1.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 0.0f,

    0.0f, 1.0f, 0.0f, 1.0f,
    1.0f, 1.0f, 1.0f, 1.0f,
    1.0f, 0.0f, 1.0f, 0.0f
  };

  glGenVertexArrays(1, &renderer->vao);
  glCheckError();
  glGenBuffers(1, &vbo);
  glCheckError();

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glCheckError();
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glCheckError();

  glBindVertexArray(renderer->vao);
  glCheckError();
  glEnableVertexAttribArray(0);
  glCheckError();
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
  glCheckError();
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glCheckError();
  glBindVertexArray(0);
  glCheckError();
}

SpriteRenderer initSpriteRenderer(GLuint shader)
{
  SpriteRenderer renderer = {
    .shader = shader,
    .vao = 0,
  };
  initRenderData(&renderer);
  return renderer;
}

void spriteRendererDrawSprite(SpriteRenderer renderer,
                              GLuint texture,
                              SpriteRendererOptions options)
{
  glUseProgram(renderer.shader);
  glCheckError();
  kmMat4 model;
  kmMat4Identity(&model);
  kmMat4Translation(&model, options.position.x, options.position.y, 0.0f);

  kmMat4Translation(&model, 0.5f * options.size.x, 0.5f * options.size.y, 0.0f);
  kmMat4RotationZ(&model, kmDegreesToRadians(options.rotate));
  kmMat4Translation(&model, -0.5f * options.size.x, -0.5f * options.size.y, 0.0f);

  kmMat4Scaling(&model, options.size.x, options.size.y, 1.0f);

  // Set uniforms in shader.
  glUniformMatrix4fv(glGetUniformLocation(renderer.shader, "model"), 1, GL_FALSE, &model.mat[0]);
  glCheckError();
  glUniform3f(glGetUniformLocation(renderer.shader, "spriteColor"),
              options.color.x, options.color.y, options.color.z);
  glCheckError();

  // Bind shader texture.
  glActiveTexture(GL_TEXTURE0);
  glCheckError();
  glBindTexture(GL_TEXTURE_2D, texture);
  glCheckError();

  // Draw vertex array.
  glBindVertexArray(renderer.vao);
  glCheckError();
  glDrawArrays(GL_TRIANGLES, 0, 6);
  glCheckError();
  glBindVertexArray(0);
  glCheckError();
}

void freeSpriteRenderer(SpriteRenderer renderer)
{
  glDeleteVertexArrays(1, &renderer.vao);
}
